import { useRouter } from 'next/router';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import { Button, PageHeader, Descriptions, Input, message } from 'antd';

import { withContextInitialized } from '../../components/hoc';
import CompanyCard from '../../components/molecules/CompanyCard';
import GenericList from '../../components/organisms/GenericList';
import OverlaySpinner from '../../components/molecules/OverlaySpinner';
import { usePersonInformation } from '../../components/hooks/usePersonInformation';

import { Company } from '../../constants/types';
import { ResponsiveListCard } from '../../constants';

const PersonDetail = () => {
  const router = useRouter();
  const { load, loading, save, data } = usePersonInformation(
    router.query?.email as string,
    true
  );

  const [isEditing, setIsEditing] = useState<boolean>(false);
  const [name, setName] = useState<string>('');
  const [gender, setGender] = useState<string>('');
  const [phone, setPhone] = useState<string>('');
  const [birthday, setBirthday] = useState<string>('');

  useEffect(() => {
    setName(data.name);
    setGender(data.gender);
    setPhone(data.phone);
    setBirthday(data.birthday);
  }, [data])

  const handleToggleEditMode = useCallback(() => setIsEditing((prevState) => !prevState), []);

  useEffect(() => {
    load();
  }, []);

  if (loading) {
    return <OverlaySpinner title={`Loading ${router.query?.email} information`} />;
  }

  if (!data) {
    message.error("The user doesn't exist redirecting back...", 2, () =>
      router.push('/home')
    );
    return <></>;
  }

  return (
    <>
      <PageHeader
        onBack={router.back}
        title="Person"
        subTitle="Profile"
        extra={[
          <Button
            style={{ padding: 0, margin: 0 }}
            type="link"
            href={data.website}
            target="_blank"
            rel="noopener noreferrer"
          >
            Visit website
          </Button>,
          <Button type="default" onClick={handleToggleEditMode}>
            {isEditing ? 'Save' : 'Edit'}
          </Button>,
        ]}
      >
        {data && (
          <div>
            {isEditing ? (
              <>
                <Input value={name} onChange={(e) => setName(e.target.value)} />
                <Input value={gender} onChange={(e) => setGender(e.target.value)} />
                <Input value={phone} onChange={(e) => setPhone(e.target.value)} />
                <Input value={birthday} onChange={(e) => setBirthday(e.target.value)} />
              </>
            ) : (
              <Descriptions size="small" column={1}>
                <Descriptions.Item label="Name">{name}</Descriptions.Item>
                <Descriptions.Item label="Gender">{gender}</Descriptions.Item>
                <Descriptions.Item label="Phone">{phone}</Descriptions.Item>

                <Descriptions.Item label="Birthday">{birthday}</Descriptions.Item>
              </Descriptions>

            )}
          </div>
        )}
        <GenericList<Company>
          loading={loading}
          extra={ResponsiveListCard}
          data={data && data.companyHistory}
          ItemRenderer={({ item }: any) => <CompanyCard item={item} />}
          handleLoadMore={() => { }}
          hasMore={false}
        />
      </PageHeader>
    </>
  );
};

export default withContextInitialized(PersonDetail);
